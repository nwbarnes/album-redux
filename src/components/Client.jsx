import React from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import AlbumContainer from "./AlbumContainer";
import PhotoContainer from "./PhotoContainer";
import HomePage from "./HomePage";

function Client() {
  return (
    <Router>
      <div>
        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>
          <Route exact path="/:albumId">
            <div>
              <AlbumContainer />
            </div>
          </Route>
          <Route exact path="/:albumId/:photoId">
            <div>
              <PhotoContainer />
            </div>
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

const mapStateToProps = (state) => {
  return {
    albums: state.albums.albums,
    photos: state.photos.photos,
    users: state.users.users,
  };
};

export default connect(mapStateToProps)(Client);
