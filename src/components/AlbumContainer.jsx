import React, { useEffect } from "react";
import { useParams, useHistory } from "react-router-dom";
import { connect } from "react-redux";
import { FetchPhotos } from "../actions";

function AlbumContainer(props) {
  let history = useHistory();
  let { albumId } = useParams();
  useEffect(() => {
    props.dispatch(FetchPhotos(albumId));
  }, {});

  const mappedPhotos = props.photos.photos.map((photo) => (
    <li className="list-group-item">
      <img
        onClick={() => {
          history.push("/" + albumId + "/" + photo.id);
        }}
        src={photo.thumbnailUrl}
        alt="thumbnail"
      />
    </li>
  ));

  return <div>{props.photos.photos.length > 0 && mappedPhotos}</div>;
}

const mapStateToProps = (state) => {
  return {
    photos: state.photos,
  };
};

export default connect(mapStateToProps)(AlbumContainer);
