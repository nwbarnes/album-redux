import React from "react";
import { useHistory } from "react-router-dom";

function AlbumItem(props) {
  let history = useHistory();
  const { users, album } = props;
  const filteredUsers = users.filter((user) => user.id == album.userId);
  console.log(filteredUsers[0].name);

  return (
    <div>
      <h5
        onClick={() => {
          history.push("/" + props.album.id + "/");
        }}
      >
        {album.title}
      </h5>
      <p>{filteredUsers[0].name}</p>
    </div>
  );
}

export default AlbumItem;
