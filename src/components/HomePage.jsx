import React from "react";
import { fetchAlbums, fetchUsers } from "../actions";
import { connect } from "react-redux";
import AlbumItem from "./AlbumItem";

class HomePage extends React.Component {
  componentDidMount() {
    if (!this.props.users.fetched) {
      this.props.dispatch(fetchUsers());
    }

    if (!this.props.albums.fetched) {
      this.props.dispatch(fetchAlbums());
    }
  }

  render() {
    const { users, albums } = this.props;
    return (
      <div>
        <ul className="list-group">
          {albums.albums.map((album) => (
            <li className="list-group-item">
              <AlbumItem album={album} users={users.users} />
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    albums: state.albums,
    users: state.users,
  };
};

export default connect(mapStateToProps)(HomePage);
