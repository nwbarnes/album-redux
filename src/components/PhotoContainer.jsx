import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { connect } from "react-redux";
import { FetchPhotos } from "../actions";

function PhotoContainer(props) {
  let { albumId, photoId } = useParams();
  useEffect(() => {
    props.dispatch(FetchPhotos(albumId));
  }, {});
  let photos = props.photos.photos.filter((photo) => photo.id == photoId);
  // let photos = props.photos.filter(
  //   (photo) => photo.albumId == albumId && photo.id == photoId
  // );
  return (
    <div>
      <img
        alt="full"
        src={photos.length > 0 && photos[0] != null && photos[0].url}
      ></img>
    </div>
  );
}

const mapStateToProps = (state) => {
  console.log("mapStateToProps in albumContainer", state);
  return {
    photos: state.photos,
  };
};

export default connect(mapStateToProps)(PhotoContainer);
