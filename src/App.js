import React from "react";
import Client from "./components/Client";
import store from "./store";
import "bootstrap/dist/css/bootstrap.min.css";
import { Provider } from "react-redux";

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Client />
      </div>
    </Provider>
  );
}

export default App;
