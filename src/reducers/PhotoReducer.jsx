export default function reducer(
  state = {
    photos: [],
    fetching: false,
    fetched: false,
    error: null,
  },
  action
) {
  switch (action.type) {
    case "FETCH_PHOTOS_PENDING": {
      return { ...state, fetching: true };
    }
    case "FETCH_PHOTOS_REJECTED": {
      return { ...state, fetching: false, error: action.payload.data };
    }
    case "FETCH_PHOTOS_FULFILLED": {
      return {
        ...state,
        fetching: false,
        fetched: true,
        photos: action.payload.data,
      };
    }
    default:
      return state;
  }
}
