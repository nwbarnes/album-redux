import { combineReducers } from "redux";

import albums from "./AlbumsReducer";
import users from "./UserReducer";
import photos from "./PhotoReducer";

export default combineReducers({ albums, users, photos });
