import axios from "axios";

export function fetchAlbum(albumId) {
  return {
    type: "FETCH_ALBUM",
    payload: axios.get("http://jsonplaceholder.typicode.com/albums/" + albumId),
  };
}

export function fetchAlbums() {
  return {
    type: "FETCH_ALBUMS",
    payload: axios.get("http://jsonplaceholder.typicode.com/albums"),
  };
}
export function fetchUsers() {
  return {
    type: "FETCH_USERS",
    payload: axios.get("http://jsonplaceholder.typicode.com/users"),
  };
}
export function fetchUser(userId) {
  return {
    type: "FETCH_USERS",
    payload: axios.get("http://jsonplaceholder.typicode.com/users/" + userId),
  };
}
export function FetchPhotos(albumId) {
  return {
    type: "FETCH_PHOTOS",
    payload: axios.get(
      "http://jsonplaceholder.typicode.com/albums/" + albumId + "/photos"
    ),
  };
}
